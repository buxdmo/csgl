#pragma once

#include <cstdint>
#include <functional>
#include <thread>
#include <list>

#ifdef _WIN32

#include <WinSock2.h>
#pragma comment(lib, "WSock32.Lib")

#define closeSocket(sckt) closesocket(sckt)
#define SOCKET_TYPE SOCKET

#else

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define closeSocket(sckt) close(sckt)
#define SOCKET_TYPE int

#endif





