#pragma once
#include "framework.hpp"

static const size_t LENGTH = 4096;

class Client {
    using socket_type = SOCKET_TYPE;
    using address_type = sockaddr_in;

public:
    FORCEINLINE explicit Client(socket_type socket, address_type address) :
        socket(socket), address(address) {}

    FORCEINLINE ~Client() {
        shutdown(socket, 0);
        closeSocket(socket);
    }

    FORCEINLINE uint32_t getHost() const {
        return address.sin_addr.s_addr;
    }

    FORCEINLINE uint16_t getPort() const {
        return address.sin_port;
    }

    FORCEINLINE int loadData() {
        return recv(socket, buffer, LENGTH, 0);
    }

    FORCEINLINE char* getData() {
        return buffer;
    }

    FORCEINLINE bool sendData(const char* buffer, size_t size) const {
        return !(send(socket, buffer, size, 0) < 0);
    }

private:
    socket_type socket{};
    address_type address{};
    char buffer[LENGTH]{};
};
