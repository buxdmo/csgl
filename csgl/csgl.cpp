﻿#include "Client.hpp"
#include "Server.hpp"
#include "csgl.hpp"

std::string getHostStr(Client&& client) {
    uint32_t host = client.getHost();
    uint8_t* ip = reinterpret_cast<uint8_t*>(&ip);
    std::stringstream ss;
    ss << ip[0] << '.' <<
        ip[1] << '.' <<
        ip[2] << '.' <<
        ip[3] << ':' <<
        client.getPort();
    return ss.str();
}

int main() {
    Server server(8080,
        [](Client&& client) {

        std::cout << "Connected host:" << getHostStr(std::forward<Client>(client)) << std::endl;

        int size = 0;
        while (!(size = client.loadData()));

        std::cout
            << "size: " << size << " bytes" << std::endl
            << client.getData() << std::endl;

        const char* answer = "Hello World from Server";
        client.sendData(answer, sizeof(answer));
        }

    );

    if (server.start() == Server::status::up) {
        std::cout << "Server is up!" << std::endl;
        server.joinLoop();
    }
    else {
        std::cout << "Server start error! Error code:" << static_cast<uint8_t>(server.getStatus()) << std::endl;
        return -1;
    }

}