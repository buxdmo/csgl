#pragma once
#include "Client.hpp"


class Server {
    using handler_function_t = std::function<void(Client&&)>;
    using socket_type = SOCKET_TYPE;
    using address_type = sockaddr_in;

public:
    enum class status : uint8_t {
        up = 0,
        err_socket_init = 1,
        err_socket_bind = 2,
        err_socket_listening = 3,
        close = 4
    };

public:
    FORCEINLINE explicit Server(uint16_t port, handler_function_t&& handler) :
        port(port), handler(handler) {}


    FORCEINLINE ~Server() {
        if (_status == status::up) stop();
#ifdef _WIN32 
        WSACleanup();
#endif
    }

    FORCEINLINE status getStatus() const {
        return _status;
    }

    FORCEINLINE void setHandler(handler_function_t&& handler) {
        this->handler = handler;
    }

    FORCEINLINE uint16_t getPort() const {
        return port;
    }

    FORCEINLINE uint16_t setPort(uint16_t port) {
        if (this->port != port) {
            this->port = port;
            restart();
        }
        return port;
    }

    FORCEINLINE status restart() {
        if (_status == status::up) {
            stop();
        }
        return start();
    }

    FORCEINLINE void joinLoop() {
        handlerThread.join();
    }


    FORCEINLINE status start() {
#ifdef _WIN32
        WSAStartup(MAKEWORD(2, 2), &wsaData);
#endif

        address_type address;
        address.sin_addr.s_addr = INADDR_ANY;
        address.sin_port = htons(port);
        address.sin_family = AF_INET;

        serverSocket = socket(AF_INET, SOCK_STREAM, 0);

        if (serverSocket == INVALID_SOCKET)
            return _status = status::err_socket_init;

        if (bind(serverSocket, reinterpret_cast<sockaddr*>(&address), sizeof(address)) == INVALID_SOCKET)
            return _status = status::err_socket_bind;

        if (listen(serverSocket, SOMAXCONN) == INVALID_SOCKET)
            return _status = status::err_socket_listening;

        _status = status::up;
        handlerThread = std::thread([this] {
            handlingLoop();
        });
        return _status;
    }

    FORCEINLINE void stop() {
        _status = status::close;
        closeSocket(serverSocket);
        joinLoop();
        for (auto& thr : clientHandlerThreads)
            thr.join();
        clientHandlerThreads.clear();
        clientHandlingEnd.clear();
    }


private:
    FORCEINLINE void handlingLoop() {
        while (_status == status::up) {
            socket_type client_socket;
            address_type client_addr;
            int addrlen = sizeof(client_addr);

            client_socket = accept(serverSocket, (struct sockaddr*)&client_addr, &addrlen);
            if (client_socket && _status == status::up) {
                clientHandlerThreads.push_back(
                    std::thread([this, &client_socket, &client_addr] {
                    handler(Client(client_socket, client_addr));
                    clientHandlingEnd.push_back(std::this_thread::get_id());
                }));
            }

            if (!clientHandlingEnd.empty())
                for (auto id_it = clientHandlingEnd.begin(); !clientHandlingEnd.empty(); id_it = clientHandlingEnd.begin())
                    for (auto thr_it = clientHandlerThreads.begin(); thr_it != clientHandlerThreads.end(); ++thr_it)
                        if (thr_it->get_id() == *id_it) {
                            thr_it->join();
                            clientHandlerThreads.erase(thr_it);
                            clientHandlingEnd.erase(id_it);
                            break;
                        }
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
        }
    }

private:
    uint16_t port{};
    handler_function_t handler{};
    status _status = status::close;

    std::thread handlerThread{};
    std::list<std::thread> clientHandlerThreads{};
    std::list<std::thread::id> clientHandlingEnd{};

    socket_type serverSocket = INVALID_SOCKET;
    WSAData wsaData{};
};